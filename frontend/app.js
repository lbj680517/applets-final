//app.js
var util = require('./utils/util.js');
const api = require('./utils/util1.js')
const interfaceApi = require('./utils/interface.js');
const util1 = require('./utils/util1.js');
// const rootUrl = 'http://localhost:8080'

App({
  onLaunch: function () {
    var that = this
    // 展示本地存储能力
    var logs = wx.getStorageSync('logs') || []
    logs.unshift(Date.now())
    wx.setStorageSync('logs', logs)
    // 登录
    wx.login({
      success: res => {
        wx.removeStorageSync('sessionid');
        // 发送 res.code 到后台换取 openId, sessionKey, unionId
        var url = interfaceApi.login
        if (res.code !== null && res.code !== '') {
          //发起网络请求,获取用户UnionID
          return new Promise((resolve, reject) => {
            api.getData(url, {
                code: res.code
              }).then((res) => {
                var data = JSON.parse(res.data)
                console.log(data.openid)
                this.globalData.openId = data.openid
                // 获取用户信息
                wx.getSetting({
                  success: res => {
                    if (res.authSetting['scope.userInfo']) {
                      // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
                      wx.getUserInfo({
                        success: res => {
                          // 可以将 res 发送给后台解码出 unionId
                          this.getUserInfo(res.userInfo)
                          // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
                          // 所以此处加入 callback 以防止这种情况
                          if (this.userInfoReadyCallback) {
                            this.userInfoReadyCallback(res)
                          }
                        }
                      })
                    } else {
                      console.log(1111111)
                    }
                  },
                })
                resolve()
              })
              .catch((err) => {
                console.error(err)
                reject(err)
              })
          })
        } else {
          //获取code失败
          console.log('登录失败！' + res.errMsg)
        }
      }
    })
  },
  /**
   * page: 0 管理员  1  普通用户
   */
  globalData: {
    userInfo: null,
    role: 0,
    openId: '',
    api,
    interfaceApi,
    page: 1
  },
  getSystemInfo: function () {
    let t = this;
    wx.getSystemInfo({
      success: function (res) {
        t.globalData.systemInfo = res;
      }
    });
  },
  async getUserInfo(e) {
    console.log("openid-------" + this.globalData.openId)

    var that = this
    await api.showLoading()
    await this.getUserInfoByOpenId(this.globalData.openId)
    await this.getRoleLevel(this.globalData.openId)
    await this.updateUserInfo(e)
    await api.hideLoading()
    if (this.globalData.userInfo === null) {
      this.getUserInfo(e)
    } else {
      wx.switchTab({
        url: '/pages/index/index',
      })
      //  wx.switchTab({
      //   url: 'pages/user/submitFeedback/submitFeedback',
      // })

      return
    }
  },

  // 获取用户信息
  getUserInfoByOpenId(e) {
    var that = this
    var info = null
    var url = interfaceApi.getUserInfoByOpenId
    var url = interfaceApi.getUserInfoByOpenId
    return new Promise((resolve, reject) => {
      api.getData(url, {
          openId: this.globalData.openId
        }).then((res) => {
          this.globalData.userInfo = res.data
          resolve()
        })
        .catch((err) => {
          console.error(err)
          reject(err)
        })
    })

  },

  /**
   * 获取用户等级  是否是管理员
   * @param {*} options 
   */
  getRoleLevel(options) {
    var url = interfaceApi.selectUserLevel
    return new Promise((resolve, reject) => {
      api.getData(url, {
          openId: options
        }).then((res) => {
          if (res.code == 200) {
            this.globalData.role = res.data
            if (res.data > 0) {
              this.globalData.page = 0
            }
          }
          resolve()
        })
        .catch((err) => {
          console.error(err)
          reject(err)
        })
    })
  },
  /**
   * 根据实时的用户信息修改数据库中的
   * @param {*} options 
   */
  updateUserInfo(options) {
    var url = interfaceApi.updateUserByOpenId + "?openId=" + this.globalData.openId
    return new Promise((resolve, reject) => {
      api.putData(url, options).then(res => {
          console.log(res)
          resolve()
        })
        .catch((err) => {
          console.error(err)
          reject(err)
        })
    })
  }
})