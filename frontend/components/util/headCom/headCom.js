// components/headCom/headCom.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    pageInfo: {
      type: Array,
      value: ["天翼智慧家庭有限公司实验室进场申请表", "/pages/enterOrder/enterOrder", ""],
      observer:function(newVal,oldVal,change){
        console.log(newVal, oldVal, change)
      }

    }
  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {
    /**
     * 返回上一个页面
     */
    returnBack() {
      wx.navigateBack()
    }
  }
})