// conponents/hdCom/hdCom.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    infoList: {
      type: Array,
      value: [150, 120, 29]
    },

    pageInfo: {
      type: Array,
      value: []
    },
    page: {
      type: Number,
      value: 0
    }
  },

  /**
   * 组件的初始数据
   */
  data: {

  },
  onLoad() {
    console.log(this.data.page)
  },
  //监听页面卸载 
  onUnload: function () {
    
  },
  /**
   * 组件的方法列表
   */
  methods: {

  }
})