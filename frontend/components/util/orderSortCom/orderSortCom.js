// components/orderSortCom/orderSortCom.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    sortNum: {
      type: Number,
      value: 0
    },
    sortInfo: {
      type: Array,
      value: [
        [0, "ascending"],
        [0, "ascending"]
      ],
      observer: function (newVal, oldVal) {
        //console.log(newVal)
      }
    },
    listData: {
      type: Array,
      value: []
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    // //当前选中的排序方式 0为按进度 1为按时间
    // sortNum: 0,
    // //progress排序的状态 0为升序  1为倒序
    // progressStatus: 0,
    // progressIcon: "ascending",
    // //progress排序的状态 0为升序  1为倒序
    // timeStatus: 0,
    // timeIcon: "ascending"
  },
  lifetimes: {
    attached: function () {
      console.log(this.data)
    }
  },
  /**
   * 组件的方法列表
   */
  methods: {
    listSort: function (e) {
      var that = this
      var id = e.currentTarget.dataset.id
      var statu = e.currentTarget.dataset.statu
      //console.log(e.currentTarget)
      if (id === '0') {
        if (statu === 0) {
          that.setData({
            sortNum: id,
            sortInfo: [
              [1, "descending"],
              [0, "ascending"]
            ]
          })
        } else if (statu === 1) {
          that.setData({
            sortNum: id,
            sortInfo: [
              [0, "ascending"],
              [0, "ascending"]
            ]
          })
        }

      }
      if (id === '1') {
        if (statu === 0) {
          that.setData({
            sortNum: id,
            sortInfo: [
              [0, "ascending"],
              [1, "descending"]
            ]
          })
        } else if (statu === 1) {
          that.setData({
            sortNum: id,
            timeStatus: 0,
            timeIcon: "descending",
            sortInfo: [
              [1, "ascending"],
              [0, "ascending"]
            ]
          })
        }
      }
      that.triggerEvent('showInfo', that.data);
    }
  }
})