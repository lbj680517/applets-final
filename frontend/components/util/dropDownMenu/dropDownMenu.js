// components/dropDownMenu/dropDownMenu.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    selection:{
      type:Array,
      value:["集团采集招标选型、到货检测测试","省公司/专业公司委托测试","公司内部事业部委托测试","产业链合作伙伴能力开放","公司内部创新孵化项目","集团及公司其他科研项目"]
    },
    kind: {
      type:String,
      value:"0"
    }
  },
  /**
   * 组件的初始数据
   */
  data: {
    select: false,
    value: ''
  },
  /**
   * 组件的方法列表
   */
  methods: {
    bindShowMsg: function() {
      this.setData({
          select:!this.data.select
      })
    },
    mySelect: function(e) {
      var name = e.currentTarget.dataset.name
      this.setData({
          value: name,
          select: false
      })
      this.triggerEvent('getValue', name);
    }
  }
})
