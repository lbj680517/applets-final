const pages = getCurrentPages()
// components/listItemCom/listItemCom.js
const app = getApp()
const api = app.globalData.api
const interfaceApi = app.globalData.interfaceApi
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    completeNum: {
      type: Number,
      value: 0,
      observer: function (newVal, oldVal) {
        console.log(newVal)
      }
    },
    listData: {
      type: Array,
      value: [],
      observer: function (newVal, oldVal) {
        console.log(pages)
      }
    }
  },


  /**
   * 组件的初始数据
   */
  data: {
    isLock: false,
    isVerified: false,
    isDel: false
  },

  /**
   * 组件的方法列表
   */
  methods: {
    //弃用
    delOrder: function (e) {
      var entryNo = e.currentTarget.dataset.entryno
      var statu = e.currentTarget.dataset.statu
      var lock = this.data.isLock
      var completeNu = this.data.completeNum
      var that = this
      if (!lock) {
        //非锁   可删
        that.checkVerified(entryNo)
        console.log(that.data.isVerified)
        if (!(statu < completeNu)) {
          //完成
          wx.showModal({
            title: '警告',
            content: '确认删除吗？',
            success(res) {
              if (res.confirm) {
                that.implementDel(entryNo)
                wx.showModal({
                  title: '提示',
                  content: '删除成功',
                  showCancel: false
                })
                console.log(that.data.isDel)
              } else if (res.cancel) {
                console.log('用户点击取消')
              }
            }
          })
        } else {
          //未完成
          wx.showModal({
            title: '警告',
            content: '该订单未完成不可删除',
            showCancel: false
          })

        }
        return
      } else {
        //锁   不可删
        wx.showModal({
          title: '警告',
          content: '该订单正在审核，不可删除',
          showCancel: false
        })
      }
    },
    //订单删除
    implementDel: function (option) {
      var that = this
      var result = false
      var url = interfaceApi.delEntryOrder + '?entryNo=' + option
      return new Promise((resolve, reject) => {
        api.deleteData(url, {}).then((res) => {
            console.log(res.code)
            if (res.code == 200) {
              wx.showModal({
                title: '提示',
                content: '删除成功',
                showCancel: false
              })
              util.sleep(500)
              that.getEntryListInfo()
            } else {
              wx.showModal({
                title: '提示',
                content: '网络错误',
                showCancel: false
              })
            }
            resolve()
          })
          .catch((err) => {
            console.error(err)
            reject(err)
          })
      })
      // wx.request({
      //   url: 'http://localhost:8081/entryInfo/delEntryOrder?entryNo=' + option,
      //   method: "DELETE",
      //   enableCache: "true",
      //   success: ((res) => {
      //     if (res.data.code == 200) {
      //       result = true
      //     }
      //   }),
      //   fail: (() => {
      //     console.log("暂时无法获取数据")
      //   }),
      //   complete: (() => {
      //     that.setData({
      //       isDel: result
      //     })
      //   }),
      // })
    },
    //检查订单是否完成
    checkVerified: function (option) {
      var that = this
      wx.request({
        url: 'http://localhost:8081/entryInfo/checkVerified?entryNo=' + option,
        method: "POST",
        enableCache: "true",
        success: ((res) => {
          console.log(res.data)
          that.setData({
            isVerified: res.data
          })
        }),
        fail: (() => {
          console.log("暂时无法获取数据")
        }),
        complete: (() => {
          //console.log(that.data.infoList)
        }),
      })
    },
    //检查订单是否上锁
    checkEntryLock: function (option) {
      var that = this
      wx.request({
        url: 'http://localhost:8081/entryInfo/checkEntryLock?entryNo=' + option,
        method: "POST",
        enableCache: "true",
        success: ((res) => {
          console.log(res.data)
          that.setState({
            isLock: res.data
          })
        }),
        fail: (() => {
          console.log("暂时无法获取数据")
        }),
        complete: (() => {
          //console.log(that.data.infoList)
        }),
      })
    },
    //弃用

    /**
     * 删除点击
     * @param {*} e 
     */
    delEntryClick: function (e) {
      console.log(e.currentTarget.dataset.entryno)
      var entryNo = e.currentTarget.dataset.entryno
      this.delEntry(entryNo)
    },
    /**
     * 查看订单实时完成状态
     * @param {*} options 
     */
    checkEntryOrderStatus: function (options) {
      var that = this
      var url = interfaceApi.checkVerified
      return new Promise((resolve, reject) => {
        api.getData(url, {
            entryNo: options
          }).then((res) => {
            console.log(res)
            that.setData({
              lock: res
            })
            resolve()
          })
          .catch((err) => {
            console.error(err)
            reject(err)
          })
      })
    },
    /**
     * 执行订单操作的所有方法
     * @param {*} e 
     */
    async delEntry(e) {
      var that = this
      await api.showLoading()
      await this.checkEntryOrderStatus(e)
      util.sleep(500)
      console.log(this.data.lock == false)
      if (this.data.lock == true) {
        wx.showModal({
          title: '警告',
          content: '确认删除吗？',
          success(res) {
            if (res.confirm) {
              that.delEntryAction(e)
            } else if (res.cancel) {
              wx.showModal({
                title: '提示',
                content: '用户取消删除',
                showCancel: false
              })
            }
          }
        })

      } else {
        wx.showModal({
          title: '提示',
          content: '订单未完成无法删除',
          showCancel: false
        })
      }
      await api.hideLoading()
    },
    /**
     * 执行订单删除动作
     * @param {*} e 
     */
    delEntryAction: function (e) {
      var that = this
      var url = interfaceApi.delEntryOrder + '?entryNo=' + e
      return new Promise((resolve, reject) => {
        api.deleteData(url, {}).then((res) => {
            console.log(res.code)
            if (res.code == 200) {
              wx.showModal({
                title: '提示',
                content: '删除成功',
                showCancel: false
              })
              util.sleep(2000)
              that.getEntryListInfo()
            } else {
              wx.showModal({
                title: '提示',
                content: '网络错误',
                showCancel: false
              })
            }
            resolve()
          })
          .catch((err) => {
            console.error(err)
            reject(err)
          })
      })
    },
    onLoad: function () {
      console.log(pages)
    }
  }
})