// components/dropDownMenu/dropDownMenu.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    selection:{
      type:Array,
      value:[{
        "id":"8879465",
        "name":"李宝杰"
      },{
        "id":"894546213",
        "name":"朱方"
      },{
        "id":"48515412",
        "name":"test1"
      }],
      observer: function (newVal, oldVal) {
        console.log(oldVal)
        console.log(newVal)
      }
    },
    kind: {
      type:String,
      value:"0"
    }
  },
  /**
   * 组件的初始数据
   */
  data: {
    select: false,
    value: ''
  },
  /**
   * 组件的方法列表
   */
  methods: {
    bindShowMsg: function() {
      this.setData({
          select:!this.data.select
      })
    },
    mySelect: function(e) {
      var item = e.currentTarget.dataset.item
      this.setData({
          value: item.name,
          select: false
      })
      this.triggerEvent('getValue',item);
    }
  }
})
