var util = require('../../../utils/util.js');
const pages = getCurrentPages()
// components/listItemCom/listItemCom.js
const app = getApp()
const api = app.globalData.api
const interfaceApi = app.globalData.interfaceApi
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    completeNum: {
      type: Number,
      value: 0,
      observer: function (newVal, oldVal) {
        console.log(newVal)
      }
    },
    listData: {
      type: Array,
      value: [],
      observer: function (newVal, oldVal) {
        console.log(pages)
      }
    }
  },


  /**
   * 组件的初始数据
   */
  data: {
    isLock: false,
    isVerified: false,
    isDel: false
  },

  /**
   * 组件的方法列表
   */
  methods: {
    /**
     * 删除点击
     * @param {*} e 
     */
    delServiceClick: function (e) {
      console.log(e.currentTarget.dataset.serviceno)
      var serviceNo = e.currentTarget.dataset.serviceno
      this.delService(serviceNo)
    },
    /**
     * 查看订单实时完成状态
     * @param {*} options 
     */
    checkServiceOrderStatus: function (options) {
      var that = this
      var url = interfaceApi.checkServiceVerified
      return new Promise((resolve, reject) => {
        api.getData(url, {
            serviceNo: options
          }).then((res) => {
            console.log(res)
            that.setData({
              lock: res.data
            })
            resolve()
          })
          .catch((err) => {
            console.error(err)
            reject(err)
          })
      })
    },
    /**
     * 执行订单操作的所有方法
     * @param {*} e 
     */
    async delService(e) {
      var that = this
      await api.showLoading()
      await this.checkServiceOrderStatus(e)
      util.sleep(500)
      console.log(this.data.lock)
      if (this.data.lock == true) {
        wx.showModal({
          title: '警告',
          content: '确认删除吗？',
          success(res) {
            if (res.confirm) {
              that.delServiceAction(e)
            } else if (res.cancel) {
              wx.showModal({
                title: '提示',
                content: '用户取消删除',
                showCancel: false
              })
            }
          }
        })
      } else {
        wx.showModal({
          title: '提示',
          content: '订单未完成无法删除',
          showCancel: false
        })
      }
      await api.hideLoading()
    },
    /**
     * 执行订单删除动作
     * @param {*} e 
     */
    delServiceAction: function (e) {
      var that = this
      var url = interfaceApi.updateServiceOrder 
      return new Promise((resolve, reject) => {
        api.putData(url, {
          serviceNo:e,
          status:0
        }).then((res) => {
            console.log(res.code)
            if (res.code == 200) {
              wx.showModal({
                title: '提示',
                content: '删除成功',
                showCancel: false
              })
              util.sleep(500)
              that.triggerEvent('delReload',res);
            } else {
              wx.showModal({
                title: '提示',
                content: '网络错误',
                showCancel: false
              })
            }
            resolve()
          })
          .catch((err) => {
            console.error(err)
            reject(err)
          })
      })
    },
    onLoad: function () {
      console.log(this.data.listData)
    }
  }
})