// pages/confirmSubmissionServiceOrder/confirmSubmissionServiceOrder.js

//获取应用实例
const app = getApp()
const api = app.globalData.api
const interfaceApi = app.globalData.interfaceApi
Page({

  /**
   * 页面的初始数据
   */
  data: {
    pageInfo: ["天翼智慧家庭科技有限公司\n实验室服务委托单", ""],
    listData: [],
    type: 0,
    personStr: ''
  },
  //表单提交
  submitOrderDetail: function () {
    console.log(typeof JSON.stringify(this.data.listData))
    this.postListData(JSON.stringify(this.data.listData))
  },
  //提交数据至后台
  //提交数据至后台
  postListData: function (options) {
    console.log(this.data.type)
    var url = ""
    console.log(this.data.type)
    if (this.data.type == 1) {
      url = interfaceApi.saveService
    } else if (this.data.type == 2) {
      url = interfaceApi.updateServiceOrder
    }
    var that = this
    if (this.data.type == 1) {
      return new Promise((resolve, reject) => {
        api.postData(url, options).then((res) => {
            console.log(res)
            if (res.code == 200) {
              wx.showModal({
                title: '提示',
                content: '添加成功',
                showCancel: false,
                success: function (res) {
                  if (res.confirm) {
                    wx.switchTab({
                      url: '/pages/serviceOrder/serviceOrder',
                    })
                  }
                }
              })
            } else {
              wx.showModal({
                title: '提示',
                content: '添加失败，请重新检查表单',
                showCancel: false
              })
            }
            resolve()
          })
          .catch((err) => {
            wx.showModal({
              title: '提示',
              content: '操作失败，请重新检查表单',
              showCancel: false
            })
            reject(err)
          })
      })
    } else if (this.data.type == 2) {
      return new Promise((resolve, reject) => {
        api.putData(url, options).then((res) => {
            console.log(res)
            if (res.code == 200) {
              wx.showModal({
                title: '提示',
                content: '修改成功',
                showCancel: false,
                success: function (res) {
                  if (res.confirm) {
                    wx.switchTab({
                      url: '/pages/serviceOrder/serviceOrder',
                    })
                  }
                }
              })
              
            } else {
              wx.showModal({
                title: '提示',
                content: '修改失败，请重新检查表单',
                showCancel: false
              })
            }
            console.log(options.entryNo)
            //that.upEntryLock(options)
            resolve()
          })
          .catch((err) => {
            wx.showModal({
              title: '提示',
              content: '操作失败，请重新检查表单',
              showCancel: false
            })
            reject(err)
          })
      })
    }

  },
  /**
   * 返回上一个页面
   */
  returnBack() {
    wx.navigateBack()
  },
  combineList: function () {
    const that = this;
    var list = this.data.listData.personReq.split("-");
    var nameList = ["首席", "资深", "高级", "中级", "初级"];
    var temp = '';
    for (var i = 0; i < 5; i++) {
      if (list[i] != "0") {
        temp += (nameList[i] + list[i] + "人");
        if (i != 4) {
          temp += "，";
        }
      }
    }
    console.log(temp);
    this.setData({
      personStr: temp
    });
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var datas = JSON.parse(options.data)
    console.log(options)
    this.setData({
      listData: datas,
      type: options.type
    })
    console.log(typeof this.data.listData)
    this.combineList();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})