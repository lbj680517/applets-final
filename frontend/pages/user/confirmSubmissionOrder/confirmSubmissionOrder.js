// pages/confirmSubmissionOrder/confirmSubmissionOrder.js
var util = require('../../../utils/util.js');
//获取应用实例
const app = getApp()
const api = app.globalData.api
const interfaceApi=app.globalData.interfaceApi
Page({

  /**
   * 页面的初始数据
   */
  data: {
    pageInfo: ["天翼智慧家庭有限公司实验室进场申请表", ""],
    listData: [],
    type: 0
  },
  //表单提交
  submitOrderDetail: function () {
    console.log(typeof JSON.stringify(this.data.listData))
    this.getListData(JSON.stringify(this.data.listData))
  },
  //提交数据至后台
  getListData: function (options) {
    console.log(this.data.type)
    var url = ""
    var method = ""
    console.log(this.data.type)
    if (this.data.type == 1) {
      url = interfaceApi.saveEntryOrder
      method = "POST"
    } else if (this.data.type == 2) {
      url = interfaceApi.updateEntryOrder
      method = "PUT"
    }
    var that = this
    if (this.data.type == 1) {
      return new Promise((resolve, reject) => {
        api.postData(url, options).then((res) => {
            console.log(res)
            if (res.status == 200) {
              wx.showModal({
                title: '提示',
                content: '添加成功',
                showCancel: false
              })
              util.sleep(3000)
              wx.switchTab({
                url: '/pages/enterOrder/enterOrder',
              })

            } else {
              wx.showModal({
                title: '提示',
                content: '添加失败，请重新检查表单',
                showCancel: false
              })
              util.sleep(2000)
            }
            resolve()
          })
          .catch((err) => {
            wx.showModal({
              title: '提示',
              content: '操作失败，请重新检查表单',
              showCancel: false
            })
            reject(err)
          })
      })
    } else if (this.data.type == 2) {
      return new Promise((resolve, reject) => {
        api.putData(url, options).then((res) => {
            console.log(res)
            if (res.status == 200) {
              wx.showModal({
                title: '提示',
                content: '修改成功',
                showCancel: false
              })
              wx.switchTab({
                url: '/pages/enterOrder/enterOrder',
              })
            } else {
              wx.showModal({
                title: '提示',
                content: '修改失败，请重新检查表单',
                showCancel: false
              })
            }
            console.log(options.entryNo)
            //that.upEntryLock(options)
            resolve()
          })
          .catch((err) => {
            wx.showModal({
              title: '提示',
              content: '操作失败，请重新检查表单',
              showCancel: false
            })
            reject(err)
          })
      })
    }

  },
  upEntryLock: function (param) {
    console.log(entryNo)
    var url = interfaceApi.updateEntryOrder
    return new Promise((resolve, reject) => {
      api.putData(url, {
          entryNo: param,
          lock: 1
        }).then((res) => {
          console.log(res)
          resolve()
        })
        .catch((err) => {
          console.error(err)
          reject(err)
        })
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var datas = JSON.parse(options.data)
    console.log(options)
    this.setData({
      listData: datas,
      type: options.type
    })
    console.log(typeof this.data.listData)
  },
 /**
   * 返回上一个页面
   */
  returnBack() {
    var pages=getCurrentPages()
    console.log(pages)
    wx.navigateBack()
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})