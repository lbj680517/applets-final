import WxValidate from '../../../utils/WxValidate';
var util = require("../../../utils/util.js")
Page({

  /**
   * 页面的初始数据
   */
  data: {
    minHour: 9,
    maxHour: 18,
    minDate: new Date().getTime(),
    maxEndHour: '',
    selectBeginDate: '',
    selectEndDate: '',
    currentDate: new Date().getTime(),
    show: false,
    show1: false,
    selectInfoList: [],
    formatter(type, value) {
      if (type === 'year') {
        return `${value}年`;
      } else if (type === 'month') {
        return `${value}月`;
      }
      return value;
    },
    listData: [],
    form: {
      entryManager: '',
      deptName: '',
      projName: '',
      contact: '',
      tel: '',
      email: '',
      staff: '',
      description: '',
      requirement: '',
      sDate: '',
      eDate: ''
    }
  },
  // 时间 - 弹出框
  showPopup() {
    this.setData({
      key: 1
    });
    this.setData({
      show: true
    });
  },
  showPopup1() {
    this.setData({
      key1: 1
    });
    this.setData({
      show1: true
    });
  },
  // 时间 - 弹出框关闭
  onClose() {
    this.setData({
      show: false
    });
  },
  onClose1() {
    this.setData({
      show1: false
    });
  },
  // 时间 - 取消按钮
  cancelFn() {
    this.setData({
      show: false
    });
  },
  cancelFn1() {
    this.setData({
      show1: false
    });
  },
  // 时间 - 当值变化时触发的事件 start
  onInput(event) {
    var newTime = new Date(event.detail);
    if (this.data.show == 0) {
      newTime = null;
    } else {
      //console.log(event.detail);
      newTime = util.formatTime(newTime);
    }
    this.setData({
      currentDate: event.detail,
      selectBeginDate: newTime
    });
  },
  onInput1(event) {
    var newTime = new Date(event.detail);
    if (this.data.show1 == 0) {
      newTime = null;
    } else {
      newTime = util.formatTime(newTime);
    }
    this.setData({
      currentDate: event.detail,
      selectEndDate: newTime
    });
  },
  // 时间 - 确定按钮
  confirmFn(e) {
    var newTime = new Date(e.detail);
    newTime = util.formatTime1(newTime, "YY-MM-DD hh-mm");
    console.log(newTime)
    var maxHour = new Date(new Date().getFullYear(), new Date().getMonth() + 1, new Date().getDate() + 2).getTime()
    this.setData({
      selectBeginDate: newTime,
      show: false,
      maxEndHour: maxHour
    });
  },
  confirmFn1(e) {
    var newTime = new Date(e.detail);
    newTime = util.formatTime1(newTime, "YY-MM-DD hh-mm");
    console.log(newTime)
    this.setData({
      selectEndDate: newTime,
      show1: false
    });
  },

  onInput(event) {
    this.setData({
      currentDate: event.detail,
    });
  },

  /**
   * 将表单数据提交到下一页
   * @param {*} e 
   */
  saveData: function (e) {
    var that = this
    let params = e.detail.value;
    console.log(this.WxValidate.checkForm(params))
    if (!this.WxValidate.checkForm(params)) {
      const error = that.WxValidate.errorList[0]
      that.showModal(error)
      return false
    }
    wx.navigateTo({
      url: '/pages/confirmSubmissionOrder/confirmSubmissionOrder?data=' + JSON.stringify(e.detail.value) + "&type=2"
    })
  },
  /**
   * 点击下一步展示错误信息
   * @param {*} error 
   */
  showModal: function (error) {
    wx.showModal({
      content: error.msg,
      showCancel: false,
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var datas = JSON.parse(options.data)
    var selectInfoLists = JSON.parse(options.selectInfoList)
    this.setData({
      listData: datas,
      selectInfoList: selectInfoLists
    })
    this.initValidate()
  },
  initValidate: function () {
    let rules = {
      description: {
        maxlength: 200
      },
      requirement: {
        maxlength: 200
      },
      sDate: {
        required: true
      }
    }

    let message = {
      description: {
        maxlength: '资源需求不能超过200个字'
      },
      requirement: {
        maxlength: '名字不能超过200个字'
      },
      sDate: {
        required: '请选择时间'
      }
    }
    //实例化当前的验证规则和提示消息
    this.WxValidate = new WxValidate(rules, message);
  },
  /**
   * 返回上一个页面
   */
  returnBack() {
    var pages=getCurrentPages()
    console.log(pages)
    wx.navigateBack()
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})