var util = require('../../../utils/util.js');
//获取应用实例
const app = getApp()
const api = app.globalData.api
const interfaceApi = app.globalData.interfaceApi
Page({

  /**
   * 页面的初始数据
   */
  data: {
    pageInfo: ["天翼智慧家庭科技有限公司\n实验室服务委托单", "", "/pages/addServiceOrder/addServiceOrder"],
    serviceNo: "",
    infoList: [],
    selectInfoList: [],
    step: 0,
    radio: 1,
    show: false,
    currentDate: new Date().getTime(),
    formatter(type, value) {
      if (type === 'year') {
        return `${value}年`;
      } else if (type === 'month') {
        return `${value}月`;
      }
      return value;
    },
    serviceAdvance: '0',
    serviceQuality: '0',
    serviceCharge: '0',
    satisfaction: '0',
    suggestions: null,
    signature: null,
    completion: null
  },
  /**
   * 表单数据 begin
   * @param {*} event 
   */
  serviceAdvanceChange(event) {
    console.log(event.detail)
    this.setData({
      serviceAdvance: event.detail
    })
  },
  serviceQualityChange(event) {
    console.log(event.detail)
    this.setData({
      serviceQuality: event.detail
    })
  },
  serviceChargeChange(event) {
    console.log(event.detail)
    this.setData({
      serviceCharge: event.detail
    })
  },
  satisfactionChange(event) {
    console.log(event.detail)
    this.setData({
      satisfaction: event.detail
    })
  },
  signatureChange(event) {
    console.log(event.detail.value)
    this.setData({
      signature: event.detail.value
    })
  },
  suggestionsChange(event) {
    console.log(event.detail.value)
    this.setData({
      suggestions: event.detail.value
    })
  },
  completionChange() {
    this.setData({
      key: 1
    });
    this.setData({
      show: true
    });
  },
  /**
   * 表单数据 end
   */

  /**
   * 提交表单
   */
  conformFeedBack() {
    console.log(this.data)
    var that = this
    var url = interfaceApi.saveFeedBack
    var data = {
      serviceNo: this.data.serviceNo,
      serviceAdvance: this.data.serviceAdvance,
      serviceQuality: this.data.serviceQuality,
      serviceCharge: this.data.serviceCharge,
      satisfaction: this.data.satisfaction,
      suggestions: this.data.suggestions,
      signature: this.data.signature,
      completion: this.data.completion
    }
    console.log(data)
    return new Promise((resolve, reject) => {
      api.postData(url, data).then((res) => {
          if (res.code == 200) {
            wx.showModal({
              title: '提示',
              content: '添加成功',
              showCancel: false,
              success (res) {
                if (res.confirm) {
                  wx.switchTab({
                    url: '/pages/serviceOrder/serviceOrder',
                  })
                }
              }
            })
          } else {
            wx.showModal({
              title: '提示',
              content: '添加失败，请重新检查表单',
              showCancel: false,
              success (res) {
                if (res.confirm) {
                  wx.switchTab({
                    url: '/pages/serviceOrder/serviceOrder',
                  })
                }
              }
            })
          }
          resolve()
        })
        .catch((err) => {
          wx.showModal({
            title: '提示',
            content: '网络错误',
            showCancel: false
          })
          util.sleep(2000)
          reject(err)
        })
    })
  },


  onClose() {
    this.setData({
      show: false
    });
  },
  cancelFn() {
    this.setData({
      show: false
    });
  },
  onInput(event) {
    var newTime = new Date(event.detail);
    if (this.data.show == 0) {
      newTime = null;
    } else {
      //console.log(event.detail);
      newTime = util.formatTime(newTime);
    }
    this.setData({
      currentDate: event.detail,
      selectBeginDate: newTime
    });
  },
  confirmFn(e) {
    var newTime = new Date(e.detail);
    newTime = util.formatTime1(newTime, "YY-MM-DD hh-mm");
    console.log(newTime)
    var maxHour = new Date(new Date().getFullYear(), new Date().getMonth() + 1, new Date().getDate() + 2).getTime()
    this.setData({
      completion: newTime,
      show: false,
      selectBeginDate: newTime
    });
  },
  cancelFn() {
    this.setData({
      show: false
    });
  },


  /**
   * 获取信息
   * @param {*} option 
   */
  getInfoList: function (option) {
    console.log(option)
    var that = this
    var url = interfaceApi.selectByServiceNo
    return new Promise((resolve, reject) => {
      api.getData(url, {
          serviceNo: option
        }).then((res) => {
          console.log(res.data)
          that.setData({
            infoList: res.data
          })
          resolve()
        })
        .catch((err) => {
          wx.showModal({
            title: '提示',
            content: '网络错误',
            showCancel: false
          })
          reject(err)
        })
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options.type)
    this.setData({
      serviceNo: options.serviceNo,
      step: options.step,
      type: options.type
    })
    console.log(options.type)
    this.getInfoList(this.data.serviceNo)
  },
  /**
   * 返回上一个页面
   */
  returnBack() {
    var pages = getCurrentPages()
    console.log(pages)
    wx.navigateBack()
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})