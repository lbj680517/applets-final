// pages/addEnterOrderNext/addEnterOrderNext.js
import WxValidate from '../../../utils/WxValidate';
var util = require("../../../utils/util.js")

Page({

  /**
   * 页面的初始数据
   */
  data: {
    listData: [],
    form: {
      serviceManager: '',
      serviceNo: '',
      currentDate:'',
      deptName: '',
      projName: '',
      contact: '',
      tel: '',
      email: '',
      description: ''
    }
  },
  /**
   * 回到上一步
   * @param {*} e 
   */
  rewind: function (e) {
    wx.navigateBack({});
  },
  /**
   * 将表单数据提交到下一页
   * @param {*} e 
   */
  saveData: function (e) {
    var that = this
    let params = e.detail.value;
    console.log(this.WxValidate.checkForm(params))
    if (!this.WxValidate.checkForm(params)) {
      const error = that.WxValidate.errorList[0]
      that.showModal(error)
      return false
    }
    wx.navigateTo({
      url: '/pages/user/addServiceOrderLater/addServiceOrderLater?data=' + JSON.stringify(e.detail.value)
    })
  },
  /**
   * 点击下一步展示错误信息
   * @param {*} error 
   */
  showModal: function (error) {
    wx.showModal({
      content: error.msg,
      showCancel: false,
    })
  },
  getServiceNo: function () {
    var that=this
    wx.request({
      url: "http://localhost:8081/util/getServiceNo",
      data: options,
      method: "POST",
      enableCache: "true",
      success: ((res) => {
        that.setData({
          serviceNo:res.data
        })
      }),
      fail: (() => {
        console.log("网络出现错误")
      }),
      complete: (() => {
        //console.log(that.data.listData)
      }),
    })

  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var datas = JSON.parse(options.data)
    this.setData({
      listData: datas
    })
    this.initValidate();
  },
  initValidate: function () {
    let rules = {
      description: {
        maxlength: 200
      }
    }
    let message = {
      description: {
        maxlength: '资源需求不能超过200个字'
      }
    }
    //实例化当前的验证规则和提示消息
    this.WxValidate = new WxValidate(rules, message);
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})