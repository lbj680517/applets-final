// pages/showOrderDetail/showOrderDetail.js
var util = require('../../../utils/util.js');
//获取应用实例
const app = getApp()
const api = app.globalData.api
const interfaceApi=app.globalData.interfaceApi
Page({

  /**
   * 页面的初始数据
   */
  data: {
    pageInfo: ["天翼智慧家庭有限公司实验室进场申请表", "", "/pages/addEnterOrder/addEnterOrder"],
    entryNo: "",
    lock: '',
    type: '',
    infoList: [],
    selectInfoList: [],
    step: 0,
    role: 0,
    radio: 1,
    userInfo:null
  },
  getInfoList: function (option) {
    console.log(option)
    var that = this
    var url= interfaceApi.getOrderInfo+'?entryNo=' + option
    return new Promise((resolve, reject) => {
      api.getData(url, option).then((res) => {
          console.log(res.data)
          that.setData({
            infoList: res.data
          })
          resolve()
        })
        .catch((err) => {
          wx.showModal({
            title: '提示',
            content: '网络错误',
            showCancel: false
          })
          reject(err)
        })
    })
  },
  /**
   * 更新操作
   * @param {*} e 
   */
  toUpdate: function (e) {
    console.log(e.target.dataset.entryno)
    var entryNo = e.target.dataset.entryno
    this.checkEntryLock(entryNo)
  },
  /**
   * 查看订单行级锁 是否锁
   * @param {*} param 
   */
  checkEntryLock: function (param) {
    var that = this
    var url =interfaceApi.checkEntryLock
    return new Promise((resolve, reject) => {
      api.getData(url, {
          entryNo: param
        }).then((res) => {
          console.log(res)
          if (res == true) {
            //that.upEntryLock(param)
            wx.redirectTo({
              url: '/pages/updateEntryOrder/updateEntryOrder?entryNo=' + param
            })
          } else {
            wx.showModal({
              title: '提示',
              content: '该订单正在审批中,请稍后再试',
              showCancel: false
            })
          }
          resolve()
        })
        .catch((err) => {
          console.error(err)
          reject(err)
        })
    })
  },
  /**
   * 更新订单锁 上锁   订单确认修改其他人不得修改
   * @param {*} param 
   */
  upEntryLock: function (param) {
    var url = interfaceApi.updateEntryOrder
    return new Promise((resolve, reject) => {
      api.putData(url, {
          entryNo: param,
          lock: 0
        }).then((res) => {
          console.log(res)
          resolve()
        })
        .catch((err) => {
          console.error(err)
          reject(err)
        })
    })
  },
  /**
   * 审核订单
   * @param {*} e 
   */
  upOrderStatus: function (e) {
    var that = this
    var options = {
      entryNo: that.data.entryNo,
      officerId: that.data.userInfo.id,
      applicantStatus: '1',
      verifyStatus: that.data.radio
    }
    var url =  interfaceApi.updateEntryOrder
    return new Promise((resolve, reject) => {
      api.putData(url, options).then((res) => {
        console.log(res)
          if (res.status == 200) {
            wx.showModal({
              title: '提示',
              content: '审批成功',
              showCancel: false
            })
            util.sleep(500)
            wx.switchTab({
              url: '/pages/enterOrder/enterOrder',
            })
          } else {
            console.log("提交失败，请重新检查表单")
          }
        })
        .catch((err) => {
          console.error(err)
          wx.showModal({
            title: '警告',
            content: '网络错误',
            showCancel: false
          })
          reject(err)
        })
    })
   },
  /**
   * 审核单选按钮修改
   */
  radiochange: function (e) {
    console.log('radio发生change事件，携带的value值为：', e.detail.value)
    this.setData({
      radio: e.detail.value
    })
  },
  /**
   * 改变用户查看状态
   */
  changeIsLook:function(param){
    var that = this
    var options = {
      entryNo: that.data.entryNo,
      isLook:1
    }
    var url =  interfaceApi.updateEntryOrder
    return new Promise((resolve, reject) => {
      api.putData(url, options).then((res) => {
        console.log(res)
          if (res.status == 200) {
            this.getInfoList(that.data.entryNo)
          } else {
            that.returnBack()
          }
        })
        .catch((err) => {
          console.error(err)
          wx.showModal({
            title: '警告',
            content: '网络错误',
            showCancel: false
          })
          reject(err)
        })
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      entryNo: options.entryNo,
      lock: options.lock,
      type: options.type,
      step: options.step,
      userInfo:app.globalData.userInfo,
      role: app.globalData.role
    })
    console.log(this.data.role)
    this.changeIsLook(this.data.entryNo)
    //this.getInfoList(this.data.entryNo)
    
  },


  /**
   * 返回上一个页面
   */
  returnBack() {
    var pages=getCurrentPages()
    console.log(pages)
    wx.navigateBack()
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})