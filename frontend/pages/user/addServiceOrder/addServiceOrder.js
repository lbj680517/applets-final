import util1 from '../../../utils/util1';
// pages/addEnterOrder/addEnterOrder.js
import WxValidate from '../../../utils/WxValidate'
var util = require('../../../utils/util.js');
//获取应用实例
const app = getApp()
const api = app.globalData.api
const interfaceApi = app.globalData.interfaceApi
Page({

  /**
   * 页面的初始数据
   */
  data: {
    orderName: '服务委托单',
    serviceNo: '',
    userInfo:null,
    currentDate: '',
    choice: '1',
    form: {
      serviceManager: '',
      serviceNo: '',
      currentDate: '',
      deptName: '',
      projName: '',
      contact: '',
      tel: '',
      email: '',
      staff: ''
    },
    pageInfo: ["天翼智慧家庭有限公司实验室服务委托单申请表", "/pages/serviceOrder/serviceOrder", ""],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var date = new Date()
    var time = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate();
    console.log(time)
    this.setData({
      orderName: options.orderName,
      currentDate: time,
      serviceManager: options.serviceManager,
      "form.currentDate": time,
      
    })
    this.getServiceNo()
    this.initValidate()
    console.log(this.data.form);
  },
  initValidate() {
    let rules = {
      serviceManager: {
        required: true,
        maxlength: 10
      },
      deptName: {
        required: true,
        maxlength: 20
      },
      projName: {
        required: true,
        maxlength: 30
      },
      contact: {
        required: true,
        maxlength: 10
      },
      tel: {
        required: true,
        tel: true
      },
      email: {
        required: true,
        email: true
      }
    }

    let message = {
      serviceManager: {
        required: '请填写接口人',
        maxlength: '名字不能超过10个字'
      },
      deptName: {
        required: '请填写单位和部门名称',
        maxlength: '名字不能超过20个字'
      },
      projName: {
        required: '请填写服务项目名称',
        maxlength: '服务项目名称不能超过20个字'
      },
      contact: {
        required: '请填写联系人名称',
        maxlength: '联系人名称不能超过20个字'
      },
      tel: {
        required: '请填写联系人电话',
        tel: '请填写正确的联系人电话'
      },
      email: {
        required: '请填写联系人邮箱',
        email: '请填写正确的联系人邮箱'
      }
    }
    //实例化当前的验证规则和提示消息
    this.WxValidate = new WxValidate(rules, message);
  },
  /**
   * 将表单数据提交到下一个页面
   * @param {*} e 
   */
  saveData: function (e) {
    var that = this
    console.log(e.detail)
    let params = e.detail.value;
    if (!this.WxValidate.checkForm(params)) {
      const error = that.WxValidate.errorList[0]
      that.showModal(error)
      return false
    }
    
    //that.data.form.serviceManager = e.detail.value.serviceManager
    console.log(e.detail.value);
    wx.navigateTo({
      url: '/pages/user/addServiceOrderNext/addServiceOrderNext?data=' + JSON.stringify(e.detail.value)
    })
  },
  showModal: function (error) {
    wx.showModal({
      content: error.msg,
      showCancel: false,
    })
  },
  /**
   * 获取订单编号
   */
  /**
   * 获取订单编号
   */
  getServiceNo: function () {
    var that = this
    var url = interfaceApi.getEntryNo
    return new Promise((resolve, reject) => {
      api.getData(url, {}).then((res) => {
          console.log(res)
          that.setData({
            serviceNo: res
          })
          resolve()
        })
        .catch((err) => {
          wx.showModal({
            title: '提示',
            content: '网络错误',
            showCancel: false
          })
          reject(err)
        })
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})