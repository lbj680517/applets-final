var util = require('../../../utils/util.js');
//获取应用实例
const app = getApp()
const api = app.globalData.api
const interfaceApi = app.globalData.interfaceApi
Page({

  /**
   * 页面的初始数据
   */
  data: {
    pageInfo: ["天翼智慧家庭科技有限公司\n实验室服务委托单", "", "/pages/addServiceOrder/addServiceOrder"],
    serviceNo: "",
    infoList: [],
    selectInfoList: [],
    step: 0,
    radio: 1,
    type: 0 //0:查看详情   1：查看进度
  },
  getInfoList: function (option) {
    console.log(option)
    var that = this
    var url = interfaceApi.selectByServiceNo
    return new Promise((resolve, reject) => {
      api.getData(url, {
          serviceNo: option
        }).then((res) => {
          console.log(res.data)
          that.setData({
            infoList: res.data
          })
          resolve()
        })
        .catch((err) => {
          wx.showModal({
            title: '提示',
            content: '网络错误',
            showCancel: false
          })
          reject(err)
        })
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options.type)
    this.setData({
      serviceNo: options.serviceNo,
      step: options.step,
      type: options.type
    })
    console.log(options.type)
    //this.getInfoList()
    this.viewActions(this.data.serviceNo)
  },
  /**
   * 查看状态（0:未查看 1:查看过）
   */
  viewActions: function (options) {
    var that = this
    var isLook=1
    var url = interfaceApi.updateServiceOrder
    return new Promise((resolve, reject) => {
      api.putData(url, {
          serviceNo: options,
          isLook:1
        }).then((res) => {
          console.log(res)
          if(res.code==200){
            that.getInfoList(options)
          }else{
            that.returnBack()
          }
          resolve()
        })
        .catch((err) => {
          wx.showModal({
            title: '提示',
            content: '网络错误',
            showCancel: false
          })
          reject(err)
        })
    })
  },
  

  /**
   * 返回上一个页面
   */
  returnBack() {
    var pages = getCurrentPages()
    console.log(pages)
    wx.navigateBack()
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})