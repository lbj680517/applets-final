// pages/addEnterOrderNext/addEnterOrderNext.js
import WxValidate from '../../../utils/WxValidate.js'
var util = require("../../../utils/util.js")
Page({

  /**
   * 页面的初始数据
   */
  data: {
    listData: [],
    personList: ['0','0','0','0','0'],
    form: {
      serviceManager: '',
      serviceNo: '',
      currentDate:'',
      deptName: '',
      projName: '',
      contact: '',
      tel: '',
      email: '',
      description: '',
      timeReq: '',
      netReq: '',
      softReq: '',
      deviceReq: '',
      personReq: '0-0-0-0-0'
    }
  },
  /**
   * 回到上一步
   * @param {*} e 
   */
  rewind: function (e) {
    wx.navigateBack({});
  },
  /**
   * 将表单数据提交到下一页
   * @param {*} e 
   */
  saveData: function (e) {
    var that = this
    let params = e.detail.value;
    console.log(this.WxValidate.checkForm(params))
    if (!this.WxValidate.checkForm(params)) {
      const error = that.WxValidate.errorList[0]
      that.showModal(error)
      return false
    }
    e.detail.value.personReq = that.data.personList[0] + '-' + that.data.personList[1] + '-' + that.data.personList[2] + '-' + that.data.personList[3] + '-' + that.data.personList[4];
    wx.navigateTo({
      url: '/pages/user/addServiceOrderFinal/addServiceOrderFinal?data=' + JSON.stringify(e.detail.value)
    })
  },
  /**
   * 点击下一步展示错误信息
   * @param {*} error 
   */
  showModal: function (error) {
    wx.showModal({
      content: error.msg,
      showCancel: false,
    })
  },
  getServiceNo: function () {
    var that=this
    wx.request({
      url: "http://localhost:8081/util/getServiceNo",
      data: options,
      method: "POST",
      enableCache: "true",
      success: ((res) => {
        that.setData({
          serviceNo:res.data
        })
      }),
      fail: (() => {
        console.log("网络出现错误")
      }),
      complete: (() => {
        //console.log(that.data.listData)
      }),
    })

  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var datas = JSON.parse(options.data)
    this.setData({
      listData: datas
    })
    this.initValidate()
  },
  initValidate: function () {
    let rules = {
      timeReq: {
        maxlength: 200
      },
      netReq: {
        maxlength: 200
      },
      softReq: {
        maxlength: 200
      },
      deviceReq: {
        maxlength: 200
      }
    }
    let message = {
      description: {
        maxlength: '时间和周期要求不能超过200个字'
      },
      netReq: {
        maxlength: '场地和网络要求不能超过200个字'
      },
      softReq: {
        maxlength: '平台和软件要求不能超过200个字'
      },
      deviceReq: {
        maxlength: '终端和仪表要求不能超过200个字'
      }
    }
    //实例化当前的验证规则和提示消息
    this.WxValidate = new WxValidate(rules, message);
  },
  /**
   * 服务人员要求：步进器绑定
   * @param {*} a：不同级别人员
   * @param {*} b：获取数值
   */
  stepper: function(event) {
    var that = this;
    var param = event.currentTarget.dataset.param;
    const name = 'personList['+param+']';
    this.setData({
      [name]: event.detail.toString()
    });
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})