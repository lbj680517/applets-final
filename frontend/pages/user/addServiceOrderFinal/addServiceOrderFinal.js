// pages/addEnterOrderFinal/addEnterOrderFinal.js
import WxValidate from '../../../utils/WxValidate.js'
var util = require('../../../utils/util.js');
//获取应用实例
const app = getApp()
const api = app.globalData.api
const interfaceApi = app.globalData.interfaceApi
Page({

  /**
   * 页面的初始数据
   */
  data: {
    listData: [],
    selectFundType: [],
    selectServiceType: [],
    userInfo:null,
    form: {
      serviceManager: '',
      serviceNo: '',
      currentDate: '',
      deptName: '',
      projName: '',
      contact: '',
      tel: '',
      email: '',
      description: '',
      timeReq: '',
      netReq: '',
      softReq: '',
      deviceReq: '',
      personReq: '',
      otherReq: '',
      serviceType: '',
      fundSupport: ''
    }
  },
  /**
   * 回到上一步
   * @param {*} e 
   */
  rewind: function (e) {
    wx.navigateBack({});
  },
  /**
   * 将表单数据提交到下一页
   * @param {*} e 
   */
  saveData: function (e) {
    var that = this
    let params = e.detail.value;
    console.log(e.detail.value)
    console.log(this.WxValidate.checkForm(params))
    if (!this.WxValidate.checkForm(params)) {
      const error = that.WxValidate.errorList[0]
      that.showModal(error)
      return false
    }
    wx.navigateTo({
      url: '/pages/user/confirmSubmissionServiceOrder/confirmSubmissionServiceOrder?data=' + JSON.stringify(e.detail.value) + "&type=1"
    })
  },
  /**
   * 获取资金支持和承担费用的内容
   * @param {*} e 
   */
  getValue: function (e) {
    var that = this;
    console.log(e.detail);
    if (e != undefined) {
      this.setData({
        "form.fundSupport": e.detail
      });
    }
  },
  /**
   * 获取服务类型的内容
   * @param {*} e 
   */
  getType: function (e) {
    console.log(e.detail);
    if (e != undefined) {
      this.setData({
        "form.serviceType": e.detail
      });
    }
    console.log(this.data.form)
  },
  /**
   * 点击下一步展示错误信息
   * @param {*} error 
   */
  showModal: function (error) {
    wx.showModal({
      content: error.msg,
      showCancel: false,
    })
  },
  /**
   * 获取资金支持和承担费用列表
   */
  getFundSupportList: function () {
    var that = this
    var url = interfaceApi.getFundSupportList
    return new Promise((resolve, reject) => {
      api.getData(url, {}).then((res) => {
          console.log(res.data)
          if (res.code == 200) {
            that.setData({
              selectFundType: res.data
            })
          }else{
            wx.showModal({
              title: '提示',
              content: '网络错误',
              showCancel: false
            })
          }
          resolve()
        })
        .catch((err) => {
          wx.showModal({
            title: '提示',
            content: '网络错误',
            showCancel: false
          })
          reject(err)
        })
    })
  },
  /**
   * 获取服务类型列表
   */
  getServiceTypeList: function () {
    var that = this
    var url = interfaceApi.getServiceTypeList
    return new Promise((resolve, reject) => {
      api.getData(url, {}).then((res) => {
          console.log(res.data)
          if (res.code == 200) {
            that.setData({
              selectServiceType: res.data
            })
          }else{
            wx.showModal({
              title: '提示',
              content: '网络错误',
              showCancel: false
            })
          }
          resolve()
        })
        .catch((err) => {
          wx.showModal({
            title: '提示',
            content: '网络错误',
            showCancel: false
          })
          reject(err)
        })
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var datas = JSON.parse(options.data)
    this.setData({
      listData: datas,
      userInfo:app.globalData.userInfo
    })
    this.initValidate()
  },
  initValidate: function () {
    let rules = {
      otherReq: {
        maxlength: 200
      },
      serviceType: {
        maxlength: 200
      }
    }
    let message = {
      otherReq: {
        maxlength: '其他资源需求不能超过200个字'
      },
      serviceType: {
        maxlength: '服务类型不能超过200个字'
      }
    }
    //实例化当前的验证规则和提示消息
    this.WxValidate = new WxValidate(rules, message);
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getFundSupportList()
    this.getServiceTypeList()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})