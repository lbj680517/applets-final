//index.js
var util = require('../../utils/util.js');
//获取应用实例
const app = getApp()
const api = app.globalData.api
const interfaceApi=app.globalData.interfaceApi
Page({
  data: {
    motto: 'Hello World',
    userInfo: {},
    hasUserInfo: false,
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
    switch: true
  },
  //事件处理函数
  bindViewTap: function () {
    wx.navigateTo({
      url: '../logs/logs'
    })
  },
  onLoad: function () {
    if (app.globalData.userInfo) {
      this.setData({
        userInfo: app.globalData.userInfo,
        hasUserInfo: true
      })
    } else if (this.data.canIUse) {
      // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
      // 所以此处加入 callback 以防止这种情况
      app.userInfoReadyCallback = res => {
        this.setData({
          userInfo: res.userInfo,
          hasUserInfo: true
        })
      }
    } else {
      // 在没有 open-type=getUserInfo 版本的兼容处理
      wx.getUserInfo({
        success: res => {
          app.globalData.userInfo = res.userInfo
          console(res.userInfo)
          this.setData({
            userInfo: res.userInfo,
            hasUserInfo: true
          })
        }
      })
    }
    //this.switchPage()
  },
  getPhoneNumber: function (e) {
    console.log(e.detail.errMsg)
    console.log(e.detail.iv)
    console.log(e.detail.encryptedData)
    if (e.detail.errMsg == 'getPhoneNumber:fail user deny') {
      wx.showModal({
        title: '提示',
        showCancel: false,
        content: '未授权',
        success: function (res) {}
      })
    } else {
      wx.showModal({
        title: '提示',
        showCancel: false,
        content: '同意授权',
        success: function (res) {}
      })
    }
  },
  /**
   * 点击按钮  获取用户信息事件
   * @param {*} e 
   */
  async getUserInfo(e) {
    if (e.detail.userInfo) {
      console.log(app.globalData.openId)
      await api.showLoading()
      this.setData({
        hasUserInfo: true
      })
      await this.getUserInfoByOpenId(app.globalData.openId)
      await this.getRoleLevel(app.globalData.openId)
      await this.updateUserInfo(e.detail.userInfo)
      await api.hideLoading()
      if (this.data.userInfo === null) {
        this.getUserInfo(e)
      } else {
        // wx.switchTab({
        //   url: 'pages/user/submitFeedback/submitFeedback',
        // })
        wx.switchTab({
          url: '/pages/index/index',
        })
        
        return
      }
    } else {
      wx.showModal({
        content: "用户取消授权",
        showCancel: false
      })
    }

  },
  // 获取用户信息
  getUserInfoByOpenId() {
    var that = this
    var info = null
    var url = interfaceApi.getUserInfoByOpenId
    return new Promise((resolve, reject) => {
      api.getData(url, {
          openId: app.globalData.openId
        }).then((res) => {
          console.log(res)
          app.globalData.userInfo = res.data
          that.setData({
            userInfo: res.data
          })
          resolve()
        })
        .catch((err) => {
          console.error(err)
          reject(err)
        })
    })
  },

  /**
   * 获取用户等级  是否是管理员
   * @param {*} options 
   */
  getRoleLevel(options) {
    var url = interfaceApi.selectUserLevel
    return new Promise((resolve, reject) => {
      api.getData(url, {
          openId: options
        }).then((res) => {
          console.log(res)
          if (res.code == 200) {
            app.globalData.role = res.data
            if(res.data>0){
              app.globalData.page=0
            }
          }
          resolve()
        })
        .catch((err) => {
          console.error(err)
          reject(err)
        })
    })
  },
  /**
   * 根据实时的用户信息修改数据库中的
   * @param {*} options 
   */
  updateUserInfo(options) {
    var url = interfaceApi.updateUserByOpenId+"?openId=" + app.globalData.openId
    var list = app.globalData.userInfo
    return new Promise((resolve, reject) => {
      api.putData(url, options).then((res) => {
          console.log(res)
          resolve()
        })
        .catch((err) => {
          console.error(err)
          reject(err)
        })
    })
  }
})