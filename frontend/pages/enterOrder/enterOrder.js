// pages/service/service.js
//index.js
var util = require('../../utils/util.js');
//获取应用实例
const app = getApp()
const api = app.globalData.api
const interfaceApi = app.globalData.interfaceApi
Page({

  /**
   * 页面的初始数据
   */
  data: {
    //当前选中的排序方式 0为按进度 1为按时间
    sortNum: 0,
    sortInfo: [
      [0, "ascending"],
      [0, "ascending"]
    ],
    infoList: [0, 0, 0],
    pageInfo: ["/pages/user/addEnterOrder/addEnterOrder?orderName=进场单", "添加进场单"],
    listData: [],
    step: 1,
    role: 0,
    userInfo: null,
    page: 0,
    statu: false
  },
  showInfo: function (e) {
    var that = this
    this.setData({
      sortNum: e.detail.sortNum,
      sortInfo: e.detail.sortInfo,
      page: app.globalData.page
    })
    console.log(e.detail.sortNum)
    console.log(e.detail.sortInfo)
    this.getListData()
  },
  delReload:function(e){
    console.log(e)
    if(e.detail.code==200){
      this.reload()
    }
  },
  getListData: function () {
    var that = this
    console.log(that.data.userInfo.id)
    var url = interfaceApi.getOrderList
    console.log("当前page" + this.data.page)
    console.log('判断' + (this.data.page === 1))
    var applicationId=null
    var adminId=null
    var stepSort=null
    var dataSort =null
    if (this.data.sortNum==0) {
      if (this.data.sortInfo[0][0] === 1) {
        stepSort="desc"
      }
    }
    if (this.data.sortNum==1) {
      if (this.data.sortInfo[1][0] === 1) {
        dataSort="desc"
      }
    }
    if (this.data.page === 1) {
      applicationId=that.data.userInfo.id
    } else {
      adminId=that.data.userInfo.id
    }
    console.log()
    var data={
      start:0,
      size:15,
      applicationId:applicationId,
      adminId:adminId,
      stepSort:stepSort,
      dataSort:dataSort
    }
    console.log(data)
    return new Promise((resolve, reject) => {
      api.postData(url, data).then((res) => {
          console.log(res.data.list)
          if(res.code==200){
            that.setData({
              listData: res.data.list
            })
          }
          resolve()
        })
        .catch((err) => {
          console.error(err)
          reject(err)
        })
    })
  },
  getInfoList: function () {
    var that = this
    var url = interfaceApi.getEntryOrderCountInfo
    var applicationId=null
    var adminId = null
    if (this.data.page === 1) {
      applicationId = that.data.userInfo.id
    } else {
      adminId = that.data.userInfo.id
    }
    return new Promise((resolve, reject) => {
      api.getData(url, {
        applicationId:applicationId,
        adminId:adminId
      }).then((res) => {
          console.log(res)
          that.setData({
            infoList: res.data
          })
          resolve()
        })
        .catch((err) => {
          wx.showModal({
            title: '提示',
            content: '网络错误',
            showCancel: false
          })
          reject(err)
        })
    })
    
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      role: app.globalData.role,
      userInfo: app.globalData.userInfo,
      page: app.globalData.page
    })
  },
  reload: function () {
    this.getListData()
    this.getInfoList()
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    console.log(this.data.sortInfo)
    console.log(this.data.sortNum)
    this.setData({
      page: app.globalData.page
    })
    this.getListData()
    this.getInfoList()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.reload()
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})