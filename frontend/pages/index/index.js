var util = require('../../utils/util.js');
//获取应用实例
const app = getApp()
const api = app.globalData.api
const interfaceApi = app.globalData.interfaceApi
Page({
  /**
   * 页面的初始数据
   */
  data: {
    active: 0,
    role: 0,
    serviceListInfo: null,
    entryListInfo: null,
    serviceCount: 0,
    entryCount: 0,
    userInfo: null,
    lock: true,
    page: 0,
    currentNotice: 0,
    notice: "正在查询您的订单哦~~~  请稍后",
    noticeList: []
  },
  /**
   * 添加按钮跳转页面
   */
  switchPage: function (e) {
    console.log(e.currentTarget.dataset.url)
    var url = e.currentTarget.dataset.url
    wx.navigateTo({
      url: url
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(app.globalData.userInfo)
    // if(app.globalData.userInfo==null){
    //   app.getUserInfo(null)
    // }
    this.setData({
      userInfo: app.globalData.userInfo,
      role: app.globalData.role,
      page: app.globalData.page
    })
  },
  /**
   * 初始化方法
   */
  async init() {
    await api.showLoading()
    await this.getEntryCount()
    await this.getServiceCount()
    await this.getEntryListInfo()
    await this.getServiceListInfo()
    //this.setNoticeList(this.data.serviceListInfo, this.data.entryListInfo)
    await api.hideLoading()
  },
  /**
   * 获取服务单数量
   */
  getServiceCount() {
    var that = this
    var url = interfaceApi.selectServiceCountByApplicationId
    var applicationId = null
    var adminId = null
    if (this.data.page === 1) {
      applicationId = that.data.userInfo.id
    }
    return new Promise((resolve, reject) => {
      api.getData(url, {
          applicationId: applicationId,
          adminId: adminId
        }).then((res) => {
          console.log(res)
          that.setData({
            serviceCount: res.data
          })
          resolve()
        })
        .catch((err) => {
          console.error(err)
          reject(err)
        })
    })
  },
  /**
   * 获取进场单数量
   */
  getEntryCount() {
    var that = this
    var url = interfaceApi.selectEntryCountByApplicationId
    var applicationId = null
    var adminId = null
    if (this.data.page === 1) {
      applicationId = that.data.userInfo.id
    }
    //  else {
    //   adminId = that.data.userInfo.id
    // }
    return new Promise((resolve, reject) => {
      api.getData(url, {
          applicationId: applicationId,
          adminId: adminId
        }).then((res) => {
          console.log(res)
          that.setData({
            entryCount: res.data
          })
          resolve()
        })
        .catch((err) => {
          console.error(err)
          reject(err)
        })
    })
  },
  /**
   * 获取服务单列表
   */
  getServiceListInfo() {
    var that = this
    var url = interfaceApi.selectServiceList
    console.log(this.data.role === '0')
    var applicationId = null
    var adminId = null
    if (this.data.page === 1) {
      applicationId = that.data.userInfo.id
    }
    // else {
    //   adminId = that.data.userInfo.id
    // }
    var data = {
      start: 0,
      size: 15,
      applicationId: applicationId,
      adminId: adminId
    }
    return new Promise((resolve, reject) => {
      api.getData(url, data).then((res) => {
          console.log(res)
          if (res.code == 200) {
            that.setData({
              serviceListInfo: res.data.list
            })
          }
          resolve()
        })
        .catch((err) => {
          console.error(err)
          reject(err)
        })
    })
  },
  /**
   * 获取进场单列表
   */
  getEntryListInfo() {
    var that = this
    var url = interfaceApi.getOrderList
    console.log(this.data.role === '0')
    var applicationId = null
    var adminId = null
    if (this.data.page === 1) {
      applicationId = that.data.userInfo.id
    } else {
      adminId = that.data.userInfo.id
    }
    var data = {
      start: 0,
      size: 15,
      applicationId: applicationId,
      adminId: adminId
    }
    return new Promise((resolve, reject) => {
      api.postData(url, data).then((res) => {
          console.log(res)
          if (res.code == 200) {
            that.setData({
              entryListInfo: res.data.list
            })
          }

          resolve()
        })
        .catch((err) => {
          console.error(err)
          reject(err)
        })
    })
  },
  /**
   * 删除点击
   * @param {*} e 
   */
  delEntryClick: function (e) {
    console.log(e.currentTarget.dataset.entryno)
    var entryNo = e.currentTarget.dataset.entryno
    this.delEntry(entryNo)
  },
  delServiceClick: function (e) {
    console.log(e.currentTarget.dataset.serviceno)
    var serviceNo = e.currentTarget.dataset.serviceno
    this.delService(serviceNo)
  },
  /**
   * 查看订单实时完成状态
   * @param {*} options 
   */
  checkEntryOrderStatus: function (options) {
    var that = this
    var url = interfaceApi.checkVerified
    return new Promise((resolve, reject) => {
      api.getData(url, {
          entryNo: options
        }).then((res) => {
          console.log(res)
          that.setData({
            lock: res
          })
          resolve()
        })
        .catch((err) => {
          console.error(err)
          reject(err)
        })
    })
  },
  checkServiceOrderStatus: function (options) {
    var that = this
    var url = interfaceApi.checkServiceVerified
    return new Promise((resolve, reject) => {
      api.getData(url, {
          serviceNo: options
        }).then((res) => {
          console.log(res)
          that.setData({
            lock: res.data
          })
          resolve()
        })
        .catch((err) => {
          console.error(err)
          reject(err)
        })
    })
  },
  /**
   * 执行订单操作的所有方法
   * @param {*} 
   */
  async delEntry(e) {
    var that = this
    await api.showLoading()
    await this.checkEntryOrderStatus(e)
    util.sleep(500)
    console.log(this.data.lock == false)
    if (this.data.lock == true) {
      wx.showModal({
        title: '警告',
        content: '确认删除吗？',
        success(res) {
          if (res.confirm) {
            that.delEntryAction(e)
          } else if (res.cancel) {
            wx.showModal({
              title: '提示',
              content: '用户取消删除',
              showCancel: false
            })
          }
        }
      })
    } else {
      wx.showModal({
        title: '提示',
        content: '订单未完成无法删除',
        showCancel: false
      })
    }
    await api.hideLoading()
  },
  async delService(e) {
    var that = this
    await api.showLoading()
    await this.checkServiceOrderStatus(e)
    util.sleep(500)
    console.log(this.data.lock)
    if (this.data.lock == true) {
      wx.showModal({
        title: '警告',
        content: '确认删除吗？',
        success(res) {
          if (res.confirm) {
            that.delServiceAction(e)
          } else if (res.cancel) {
            wx.showModal({
              title: '提示',
              content: '用户取消删除',
              showCancel: false
            })
          }
        }
      })
    } else {
      wx.showModal({
        title: '提示',
        content: '订单未完成无法删除',
        showCancel: false
      })
    }
    await api.hideLoading()
  },
  /**
   * 执行订单删除动作
   * @param {*} e 
   */
  delEntryAction: function (e) {
    var that = this
    var url = interfaceApi.delEntryOrder + '?entryNo=' + e
    return new Promise((resolve, reject) => {
      api.deleteData(url, {}).then((res) => {
          console.log(res.code)
          if (res.code == 200) {
            wx.showModal({
              title: '提示',
              content: '删除成功',
              showCancel: false
            })
            util.sleep(500)
            that.getEntryListInfo()
          } else {
            wx.showModal({
              title: '提示',
              content: '网络错误',
              showCancel: false
            })
          }
          resolve()
        })
        .catch((err) => {
          console.error(err)
          reject(err)
        })
    })
  },
  delServiceAction: function (e) {
    var that = this
    var url = interfaceApi.updateServiceOrder
    return new Promise((resolve, reject) => {
      api.putData(url, {
          serviceNo: e,
          status: 0
        }).then((res) => {
          console.log(res.code)
          if (res.code == 200) {
            wx.showModal({
              title: '提示',
              content: '删除成功',
              showCancel: false
            })
            util.sleep(500)
            that.getServiceListInfo()
          } else {
            wx.showModal({
              title: '提示',
              content: '网络错误',
              showCancel: false
            })
          }
          resolve()
        })
        .catch((err) => {
          console.error(err)
          reject(err)
        })
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },
  /**
   * 获取通知信息列表
   * @param {*} serviceOrderList 
   * @param {*} entryOrderList 
   */
  setNoticeList: function (serviceOrderList, entryOrderList) {
    var that = this
    if (app.globalData.page === 0) {
      this.adminNotice(serviceOrderList, entryOrderList)
    } else {
      this.userNotice(serviceOrderList, entryOrderList)
    }
    setInterval(function () {
      that.setData({
        notice: that.data.noticeList[that.data.currentNotice],
        currentNotice: that.data.currentNotice + 1
      })
      console.log(that.data.noticeList[that.data.currentNotice - 1])
      if (that.data.currentNotice >= that.data.noticeList.length) {
        that.setData({
          currentNotice: 0
        })
      }
    }, 12000)
  },

  /**
   * 小喇叭通知---管理员
   * @param {*} serviceOrderList 
   * @param {*} entryOrderList 
   */
  adminNotice: function (serviceOrderList, entryOrderList) {
    console.log("admin")
    var serviceOrderNumber = 0
    var entryOrderNumber = 0
    serviceOrderList.forEach(element => {
      if (element.verifyStatus < 3) {
        if (element.currentReviewerId === null || element.currentReviewerId === '' || element.currentReviewerId === this.data.userInfo.id) {
          serviceOrderNumber++
        }
      }
    });
    entryOrderList.forEach(element => {
      if (element.verifyStatus === '0') {
        entryOrderNumber++
      }
    });
    console.log("serviceOrderNumber=" + serviceOrderNumber)
    console.log("entryOrderNumber=" + entryOrderNumber)
    if (serviceOrderNumber > 0) {
      this.data.noticeList.push("尊敬的管理员,您好,您有" + serviceOrderNumber + "条新服务委托单等待审核！")
    }
    if (entryOrderNumber > 0) {
      this.data.noticeList.push("尊敬的管理员,您好,您有" + entryOrderNumber + "条新进场单等待审核！")
    }
    if (!(serviceOrderNumber > 0 && entryOrderNumber > 0)) {
      this.data.noticeList.push("尊敬的管理员,您好暂无订单要处理哦~~~")
    }
  },
  /**
   * 小喇叭通知---用户
   * @param {*} serviceOrderList 
   * @param {*} entryOrderList 
   */
  userNotice: function (serviceOrderList, entryOrderList) {
    console.log("user")
    var serviceOrderPassNumber = 0
    var serviceOrderAllPassNumber = 0
    var entryOrderNumber = 0
    serviceOrderList.forEach(element => {
      if (element.isLook === 0) {
        if (element.verifyStatus === 3) {
          serviceOrderAllPassNumber++
        }
        if (element.verifyStatus < 3) {
          serviceOrderPassNumber++
        }
      }

    });
    entryOrderList.forEach(element => {
      if (element.verifyStatus > 0 && element.isLook == 0) {
        entryOrderAllPassNumber++
      }
    });
    if (serviceOrderAllPassNumber > 0) {
      this.data.noticeList.push("尊敬的用户,您好,您有" + serviceOrderAllPassNumber + "条新服务委托单完成审核！")
    }
    if (serviceOrderPassNumber > 0) {
      this.data.noticeList.push("尊敬的管理员,您好,您有" + serviceOrderPassNumber + "条新服务委托单通过一项审核！")
    }
    if (entryOrderNumber > 0) {
      this.data.noticeList.push("尊敬的用户,您好,您有" + entryOrderAllPassNumber + "条进场单已通过审核！")
    }
    if (!(serviceOrderAllPassNumber > 0 && serviceOrderAllPassNumber > 0&& entryOrderNumber > 0)) {
      this.data.noticeList.push("尊敬的用户,您好暂无订单要处理哦~~~")
    }
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.setData({
      userInfo: app.globalData.userInfo,
      role: app.globalData.role,
      page: app.globalData.page
    })
    this.init()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.init()
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})