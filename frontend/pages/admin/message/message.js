var util = require('../../../utils/util.js');
const util1 = require('../../../utils/util1.js');
//获取应用实例
const app = getApp()
const api = app.globalData.api
const interfaceApi = app.globalData.interfaceApi
Page({

  /**
   * 页面的初始数据
   */
  data: {
    entryListInfo: null,
    serviceListInfo: null,
    role: 1,
    page: 1,
    userInfo: null,
    isLook: 0
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  async init() {
    await util1.showLoading()
    if (this.data.role === '2' || this.data.role === '3') {
      await this.getServiceListInfo()
    }
    if (this.data.role === '1' || this.data.role === '3') {
      await this.getEntryListInfo()
    }
    await util1.hideLoading()
    console.log(this.data.entryListInfo)
  },
  /**
   * 获取服务单列表
   */
  getServiceListInfo() {
    var that = this
    var url = interfaceApi.selectServiceList
    var currentReviewerId = that.data.userInfo.id
    var data = {
      start: 0,
      size: 15,
      currentReviewerId: currentReviewerId
    }
    return new Promise((resolve, reject) => {
      api.getData(url, data).then((res) => {
          console.log(res.data.list)
          if (res.code == 200) {
            that.setData({
              serviceListInfo: res.data.list
            })
          }
          resolve()
        })
        .catch((err) => {
          console.error(err)
          reject(err)
        })
    })
  },
  /**
   * 获取进场单列表
   */
  getEntryListInfo() {
    // if (this.data.role !== '1') {
    //   return
    // }
    var that = this
    var url = interfaceApi.getOrderList
    var applicationId = that.data.userInfo.id
    var data = {
      start: 0,
      size: 15,
      status: 0
    }
    return new Promise((resolve, reject) => {
      api.postData(url, data).then((res) => {
          console.log(res.data.list)
          if (res.code == 200) {
            that.setData({
              entryListInfo: res.data.list
            })
          }
          resolve()
        })
        .catch((err) => {
          console.error(err)
          reject(err)
        })
    })
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.setData({
      userInfo: app.globalData.userInfo,
      role: app.globalData.role,
      page: app.globalData.page
    })
    this.init()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})