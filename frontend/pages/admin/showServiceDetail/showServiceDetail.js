var util = require('../../../utils/util.js');
const util1 = require('../../../utils/util1.js');
//获取应用实例
const app = getApp()
const api = app.globalData.api
const interfaceApi = app.globalData.interfaceApi
Page({

  /**
   * 页面的初始数据
   */
  data: {
    pageInfo: ["天翼智慧家庭科技有限公司\n实验室服务委托单", "", "/pages/addServiceOrder/addServiceOrder"],
    serviceNo: "",
    infoList: null,
    selectInfoList: null,
    selectReviewer: null,
    step: 0,
    radio: 0,
    type: 0, //0:查看详情   1：查看进度
    isShow: true,
    userInfo: null,
    role:0,
    form: {
      serviceNo: null,
      deptAdvice: null,
      deptOfficerId: null,
      labAdvice: null,
      labOfficerId: null,
      chAdvice: null,
      chOfficerId: null
    }
  },
  getInfoList: function (option) {
    console.log(option)
    var that = this
    var url = interfaceApi.selectByServiceNo
    return new Promise((resolve, reject) => {
      api.getData(url, {
          serviceNo: option
        }).then((res) => {
          console.log(res.data)
          that.setData({
            infoList: res.data
          })
          resolve()
        })
        .catch((err) => {
          wx.showModal({
            title: '提示',
            content: '网络错误',
            showCancel: false
          })
          reject(err)
        })
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options.type)
    this.setData({
      serviceNo: options.serviceNo,
      "form.serviceNo": options.serviceNo,
      step: options.step,
      type: options.type,
      isShow: options.isShow,
      userInfo: app.globalData.userInfo
    })
    console.log(options.type)
    this.getInfoList(this.data.serviceNo)
    this.selectReviewerList()
  },
  /**
   * 返回上一个页面
   */
  returnBack() {
    var pages = getCurrentPages()
    console.log(pages)
    wx.navigateBack()
  },
  /**
   * 审核单选按钮修改
   */
  radiochange: function (e) {
    console.log('radio发生change事件，携带的value值为：', e.detail.value)
    this.setData({
      radio: e.detail.value,
      "form.deptAdvice": e.detail.value,
      "form.deptOfficerId": this.data.userInfo.id
    })
  },
  radiochange1: function (e) {
    console.log('22222 radio发生change事件，携带的value值为：', e.detail.value)
    console.log(this.data.userInfo)

    this.setData({
      radio: e.detail.value,
      "form.labAdvice": e.detail.value,
      "form.labOfficerId": this.data.userInfo.id
    })
  },
  radiochange2: function (e) {
    console.log('111 radio发生change事件，携带的value值为：', e.detail.value)
    console.log(this.data.userInfo)
    this.setData({
      radio: e.detail.value,
      "form.chAdvice": e.detail.value,
      "form.chOfficerId": this.data.userInfo.id
    })
  },
  /**
   * 获取用户转发的审批人id
   * @param {*} e 
   */
  getValue1: function (e) {
    var that = this;
    console.log(e.detail);
    if (e != undefined) {
      this.setData({
        "form.labOfficerId": e.detail.id
      });
    }
  },
  getValue2: function (e) {
    var that = this;
    console.log(e.detail);
    if (e != undefined) {
      this.setData({
        "form.chOfficerId": e.detail.id
      });
    }
  },
  /**
   * 查询审查人
   */
  selectReviewerList: function () {
    var that = this
    var url = interfaceApi.selectReviewerList
    var data = {
      isReviewerList: true,
      name: 0
    }
    //notUserId: this.data.userInfo.id
    return new Promise((resolve, reject) => {
      api.getData(url, data).then((res) => {
          console.log(res.data)
          if (res.code == 200) {
            this.setData({
              selectReviewer: res.data
            })
          }
          resolve()
        })
        .catch((err) => {
          wx.showModal({
            title: '提示',
            content: '网络错误',
            showCancel: false
          })
          reject(err)
        })
    })
  },
  /**
   * 审核订单
   * @param {*} e 
   */
  async reviewOrder(e) {
    console.log(e.currentTarget.dataset.serviceno)
    console.log(this.data.radio)
    console.log(this.data.form)
    await util1.showLoading()
    await this.getRoleLevel(this.data.userInfo.id)
    await this.checkVerified(e.currentTarget.dataset.serviceno)
    await util1.hideLoading()
  },
  /**
   * 检查订单状态
   */
  checkVerified: function (param) {
    var that = this
    var url = interfaceApi.checkServiceVerified
    return new Promise((resolve, reject) => {
      api.getData(url, {
          serviceNo: param
        }).then((res) => {
          console.log(res)
          if (res.data) {
            //that.upEntryLock(param)
            // wx.redirectTo({
            //   url: '/pages/updateEntryOrder/updateEntryOrder?entryNo=' + param
            // })
            that.realTimeStatus(param)
          } else {
            wx.showModal({
              title: '提示',
              content: '该订单正在审批中,请稍后再试',
              showCancel: false,
              success: function (res) {
                if (res.confirm) {
                  that.returnBack()
                }
              }
            })
          }
          resolve()
        })
        .catch((err) => {
          console.error(err)
          reject(err)
        })
    })
  },
  /**
   * 查询订单是否审核中或者订单是否被修改
   */
  realTimeStatus: function (option) {
    var that = this
    this.getInfoList(option)
    if (this.data.infoList == null) {
      wx.showModal({
        title: '提示',
        content: '该订单正在修改中,请稍后再试',
        showCancel: false,
        success: function (res) {
          if (res.confirm) {
            that.returnBack()
          }
        }
      })
      return
    } else {
      if (this.data.infoList.verifyStatus !== 0) {
        this.checkReviewer(option,this.data.userInfo.id)
      }
    }
  },
  /**
   * 检查是否是审查人
   */
  checkReviewer: function (serviceNo,userId) {
    var that = this
    var url = interfaceApi.isReviewer
    var data = {
      serviceNo: serviceNo,
      reviewerId: userId
    }
    return new Promise((resolve, reject) => {
      api.getData(url, data).then((res) => {
          console.log(res.data)
          if (res.code == 200) {
            that.performReview()
          }else{
            wx.showModal({
              title: '提示',
              content: '您不是该订单审核人，无法审核',
              showCancel: false
            })
          }
          resolve()
        })
        .catch((err) => {
          wx.showModal({
            title: '提示',
            content: '网络错误,请稍后重试',
            showCancel: false,
            success: function (res) {
              if (res.confirm) {
                that.returnBack()
              }
            }
          })
          reject(err)
        })
    })
  },
  /**
   * 执行审查动作
   */
  performReview() {
    var that = this
    var url = interfaceApi.updateAudit
    return new Promise((resolve, reject) => {
      api.putData(url, this.data.form).then((res) => {
          console.log(res.data)
          if (res.code == 200) {
            wx.showModal({
              title: '提示',
              content: '审核成功,点击确认返回！',
              showCancel: false,
              success: function (res) {
                if (res.confirm) {
                  that.returnBack()
                }
              }
            })
          }else{
            wx.showModal({
              title: '提示',
              content: '修改失败,请稍后重试！',
              showCancel: false,
              success: function (res) {
                if (res.confirm) {
                  that.returnBack()
                }
              }
            })
          }
          resolve()
        })
        .catch((err) => {
          wx.showModal({
            title: '提示',
            content: '网络错误,请稍后重试',
            showCancel: false,
            success: function (res) {
              if (res.confirm) {
                that.returnBack()
              }
            }
          })
          reject(err)
        })
    })
  },
    /**
   * 获取用户等级  是否是管理员
   * @param {*} options 
   */
  getRoleLevel(options) {
    var url = interfaceApi.selectUserLevel
    return new Promise((resolve, reject) => {
      api.getData(url, {
          openId: options
        }).then((res) => {
          if (res.code == 200) {
            this.data.role = res.data
          }
          resolve()
        })
        .catch((err) => {
          console.error(err)
          reject(err)
        })
    })
  },
    /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})