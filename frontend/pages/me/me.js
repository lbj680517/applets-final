// pages/me/me.js
import util1 from '../../utils/util1';
var util = require('../../utils/util.js');
//获取应用实例
const app = getApp()
const api = app.globalData.api
const interfaceApi = app.globalData.interfaceApi
Page({

  /**
   * 页面的初始数据
   */
  data: {
    userInfo: null,
    show: false,
    currentPageName: '',
    page: 1,
    actions: [{
        name: '管理员界面',
        value: 0
      },
      {
        name: '用户界面',
        value: 1
      }
    ],
    role: 0,
    messageNumber: 0
  },

  showPopup() {
    this.setData({
      show: true
    });
  },
  onClose() {
    this.setData({
      show: false
    });
  },

  onSelect(event) {
    console.log(event.detail);
    app.globalData.page = event.detail.value
    this.setData({
      page: event.detail.value,
      currentPageName: event.detail.name,
      messageNumber: 0
    })
    if (event.detail.value === 0) {
      this.adminNoticeNumber(this.data.userInfo)
    } else {
      this.userNoticeNumber(this.data.userInfo)
    }
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var role = app.globalData.role
    var page = app.globalData.page
    var pageName = null
    if (page == 0) {
      pageName = "管理员界面"
    } else if (page == 1) {
      pageName = "普通用户界面"
    }
    this.setData({
      userInfo: app.globalData.userInfo,
      page: page,
      currentPageName: pageName,
      role: role
    })
    if (this.data.page === 0) {
      this.adminNoticeNumber(this.data.userInfo)
    } else {
      console.log("1111")
      console.log(this.data.userInfo)
      this.userNoticeNumber(this.data.userInfo)
    }
  },

  /**
   * 获取通知信息列表
   * @param {*} serviceOrderList 
   * @param {*} entryOrderList 
   */
  setNoticeList: function (serviceOrderList, entryOrderList) {

  },

  /**
   * 条数---管理员
   * @param {*} serviceOrderList 
   * @param {*} entryOrderList 
   */
  async adminNoticeNumber(param) {
    await util1.showLoading()
    if (this.data.role === '2' || this.data.role === '3') {
      await this.getAdminServiceNumber(param)
    }
    if (this.data.role === '1' || this.data.role === '3') {
      await this.getAdminEntryNumber(param)
    }
    console.log(this.data.messageNumber)
    await util1.hideLoading()

  },
  getAdminServiceNumber: function (param) {
    var that = this
    var url = interfaceApi.selectUserServiceOrderCount
    console.log(interfaceApi)
    console.log(param)
    console.log(param.id)
    return new Promise((resolve, reject) => {
      api.getData(url, {
          currentReviewerId: param.id,
          isAdmin:true
        }).then((res) => {
          console.log(res)
          if (res.code == 200) {
            that.setData({
              messageNumber: that.data.messageNumber + res.data
            })
          }
          resolve()
        })
        .catch((err) => {
          console.error(err)
          reject(err)
        })
    })
  },
  getAdminEntryNumber: function () {
    var that = this
    var url = interfaceApi.selectUserEntryOrderCount
    console.log(url)
    return new Promise((resolve, reject) => {
      api.getData(url, {
          role: app.globalData.role,
          isAdmin:true
        }).then((res) => {
          console.log(res)
          if (res.code == 200) {
            that.setData({
              messageNumber: that.data.messageNumber + res.data
            })
          }
          resolve()
        })
        .catch((err) => {
          console.error(err)
          reject(err)
        })
    })
  },

  /**
   * 条数---用户
   * @param {*} serviceOrderList 
   * @param {*} entryOrderList 
   */
  async userNoticeNumber(param) {
    console.log(param)
    await util1.showLoading()
    await this.getUserServiceNumber(param)
    await this.getUserEntryNumber(param)
    await util1.hideLoading()
  },
  getUserServiceNumber: function (param) {
    console.log(param)
    var that = this
    var url = interfaceApi.selectUserServiceOrderCount
    return new Promise((resolve, reject) => {
      api.getData(url, {
          applicationId: param.id,
          isLook: 0
        }).then((res) => {
          console.log(res)
          if (res.code == 200) {
            that.setData({
              messageNumber: that.data.messageNumber + res.data
            })
          }
          resolve()
        })
        .catch((err) => {
          console.error(err)
          reject(err)
        })
    })
  },
  getUserEntryNumber: function (param) {
    console.log(param)
    var that = this
    var url = interfaceApi.selectUserEntryOrderCount
    return new Promise((resolve, reject) => {
      api.getData(url, {
          applicationId: param.id,
          isLook: 0
        }).then((res) => {
          console.log(res)
          if (res.code == 200) {
            that.setData({
              messageNumber: that.data.messageNumber + res.data
            })
          }
          resolve()
        })
        .catch((err) => {
          console.error(err)
          reject(err)
        })
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  test: function (e) {
    console.log(e.detail.errMsg)
    console.log(e.detail.iv)
    console.log(e.detail.encryptedData)
    if (e.detail.errMsg == 'getPhoneNumber:fail user deny') {
      wx.showModal({
        title: '提示',
        showCancel: false,
        content: '未授权',
        success: function (res) {}
      })
    } else {
      wx.showModal({
        title: '提示',
        showCancel: false,
        content: '同意授权',
        success: function (res) {}
      })
    }
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})